package uk.co.cablepost.autoworkstations.auto_furnace;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventory;
import net.minecraft.inventory.SimpleInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.recipe.AbstractCookingRecipe;
import net.minecraft.recipe.RecipeType;
import net.minecraft.recipe.book.RecipeBookCategory;
import net.minecraft.screen.*;
import net.minecraft.screen.slot.Slot;
import uk.co.cablepost.autoworkstations.AutoWorkstations;


public class AutoFurnaceScreenHandler extends ScreenHandler {

    public Inventory inventory;
    public PropertyDelegate propertyDelegate;

    public AutoFurnaceScreenHandler(int syncId, PlayerInventory playerInventory) {
        this(syncId, playerInventory, new SimpleInventory(27), new ArrayPropertyDelegate(AutoFurnaceBlockEntity.PROPERTY_DELEGATE_SIZE));
    }

    public AutoFurnaceScreenHandler(int syncId, PlayerInventory playerInventory, Inventory inventory, PropertyDelegate propertyDelegate) {
        super(AutoWorkstations.AUTO_FURNACE_SCREEN_HANDLER, syncId);
        this.inventory = inventory;
        this.propertyDelegate = propertyDelegate;
        this.addProperties(this.propertyDelegate);

        //some inventories do custom logic when a player opens it.
        inventory.onOpen(playerInventory.player);

        //Input to smelt
        this.addSlot(new Slot(inventory, 0, 35, 19));

        //input fuel
        this.addSlot(new Slot(inventory, 1, 35, 73));

        //input empty glass bottle
        this.addSlot(new Slot(inventory, 3, 124, 19));

        //output item
        this.addSlot(new Slot(inventory, 2, 95, 46));

        //output xp bottle
        this.addSlot(new Slot(inventory, 4, 124, 73));

        //The player inventory
        for (int m = 0; m < 3; ++m) {
            for (int l = 0; l < 9; ++l) {
                this.addSlot(new Slot(playerInventory, l + m * 9 + 9, 8 + l * 18, 102 + m * 18));
            }
        }

        //The player Hotbar
        for (int m = 0; m < 9; ++m) {
            this.addSlot(new Slot(playerInventory, m, 8 + m * 18, 160));
        }
    }

    // Shift + Player Inv Slot
    @Override
    public ItemStack quickMove(PlayerEntity player, int invSlot) {
        //TODO - make only go into input slots (9 - 17)

        ItemStack newStack = ItemStack.EMPTY;
        Slot slot = this.slots.get(invSlot);
        if (slot.hasStack()) {
            ItemStack originalStack = slot.getStack();
            newStack = originalStack.copy();
            if (invSlot < this.inventory.size()) {
                if (!this.insertItem(originalStack, this.inventory.size(), this.slots.size(), true)) {
                    return ItemStack.EMPTY;
                }
            } else if (!this.insertItem(originalStack, 0, this.inventory.size(), false)) {
                return ItemStack.EMPTY;
            }

            if (originalStack.isEmpty()) {
                slot.setStack(ItemStack.EMPTY);
            } else {
                slot.markDirty();
            }
        }

        return newStack;
    }



    @Override
    public boolean canUse(PlayerEntity player) {
        return this.inventory.canPlayerUse(player);
    }

    public int getCookProgress() {
        int i = this.propertyDelegate.get(2);
        int j = this.propertyDelegate.get(3);
        if (j == 0 || i == 0) {
            return 0;
        }
        return i * 24 / j;
    }

    public int getFuelProgress() {
        int fuelTime = this.propertyDelegate.get(1);
        if (fuelTime == 0) {
            fuelTime = 200;
        }
        return this.propertyDelegate.get(0) * 24 / fuelTime;
    }

    public boolean isBurning() {
        return this.propertyDelegate.get(0) > 0;
    }

    public int getXpFillProgress(){
        int xpFillBottleProgress = this.propertyDelegate.get(4);
        int xpFillBottleMaxProgress = this.propertyDelegate.get(5);

        if(xpFillBottleMaxProgress == 0){
            return 0;
        }

        return 26 * xpFillBottleProgress / xpFillBottleMaxProgress;
    }

    public float getXpBottlesCouldFill(){
        int expTankInt = this.propertyDelegate.get(6);
        int expTankDec100 = this.propertyDelegate.get(7);

        float value = (expTankInt + ((float)expTankDec100 / 100f)) / AutoWorkstations.EXPERIENCE_BOTTLE_VALUE;
        return Math.round(value * 100f) / 100f;
    }
}
