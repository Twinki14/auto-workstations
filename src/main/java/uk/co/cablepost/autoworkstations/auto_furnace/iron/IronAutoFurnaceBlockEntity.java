package uk.co.cablepost.autoworkstations.auto_furnace.iron;

import net.minecraft.block.BlockState;
import net.minecraft.util.math.BlockPos;
import uk.co.cablepost.autoworkstations.AutoWorkstations;
import uk.co.cablepost.autoworkstations.auto_furnace.AutoFurnaceBlockEntity;

public class IronAutoFurnaceBlockEntity extends AutoFurnaceBlockEntity {
    public IronAutoFurnaceBlockEntity(BlockPos blockPos, BlockState blockState) {
        super(AutoWorkstations.IRON_AUTO_FURNACE_BLOCK_ENTITY, blockPos, blockState);
        xpFillBottleMaxProgress = 20;
        cookSpeedMul = 1;
    }
}
