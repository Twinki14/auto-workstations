package uk.co.cablepost.autoworkstations.auto_furnace;

import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.gui.screen.ingame.AbstractFurnaceScreen;
import net.minecraft.client.gui.screen.ingame.HandledScreen;
import net.minecraft.client.gui.screen.recipebook.AbstractFurnaceRecipeBookScreen;
import net.minecraft.client.render.GameRenderer;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.screen.AbstractFurnaceScreenHandler;
import net.minecraft.screen.FurnaceScreenHandler;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import uk.co.cablepost.autoworkstations.AutoWorkstations;
import uk.co.cablepost.autoworkstations.auto_crafting_table.AutoCraftingTableScreenHandler;

public class AutoFurnaceScreen extends HandledScreen<AutoFurnaceScreenHandler> {
    //A path to the gui texture. In this example we use the texture from the dispenser
    private static final Identifier TEXTURE = new Identifier(AutoWorkstations.MOD_ID, "textures/gui/container/auto_furnace.png");

    public AutoFurnaceScreen(AutoFurnaceScreenHandler handler, PlayerInventory inventory, Text title) {
        super(handler, inventory, title);
        this.backgroundWidth = 176;
        this.backgroundHeight = 184;
        this.playerInventoryTitleY = 92;
    }

    @Override
    protected void drawBackground(MatrixStack matrices, float delta, int mouseX, int mouseY) {
        RenderSystem.setShader(GameRenderer::getPositionTexProgram);
        RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
        RenderSystem.setShaderTexture(0, TEXTURE);
        int x = (width - backgroundWidth) / 2;
        int y = (height - backgroundHeight) / 2;
        drawTexture(matrices, x, y, 0, 0, backgroundWidth, backgroundHeight);

        if (handler.isBurning()) {
            int fuelProgress = handler.getFuelProgress();
            drawTexture(matrices, x + 34, y + 42 + 24 - fuelProgress, 176, 31 + 24 - fuelProgress, 17, fuelProgress);
        }

        int cookProgress = (handler).getCookProgress();
        drawTexture(matrices, x + 58, y + 46, 176, 14, cookProgress + 1, 16);

        int xpFillProgress = handler.getXpFillProgress();
        drawTexture(matrices, x + 133, y + 41, 176, 55, 18, xpFillProgress);

        textRenderer.draw(matrices, Text.of("x" + handler.getXpBottlesCouldFill()), x + 126, y + 50, 0x404040);
        textRenderer.draw(matrices, Text.of("x" + handler.getXpBottlesCouldFill()), x + 125, y + 49, 0x33de00);
    }

    @Override
    public void render(MatrixStack matrices, int mouseX, int mouseY, float delta) {
        renderBackground(matrices);
        super.render(matrices, mouseX, mouseY, delta);
        drawMouseoverTooltip(matrices, mouseX, mouseY);
    }

    @Override
    protected void init() {
        super.init();
        // Center the title
        titleX = (backgroundWidth - textRenderer.getWidth(title)) / 2;
    }
}