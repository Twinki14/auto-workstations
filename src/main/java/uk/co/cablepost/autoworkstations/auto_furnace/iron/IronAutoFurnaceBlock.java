package uk.co.cablepost.autoworkstations.auto_furnace.iron;

import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityTicker;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.item.ItemStack;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import uk.co.cablepost.autoworkstations.AutoWorkstations;
import uk.co.cablepost.autoworkstations.auto_furnace.AutoFurnaceBlock;

import java.util.List;

import static uk.co.cablepost.autoworkstations.util.TextUtil.translatableText;

public class IronAutoFurnaceBlock extends AutoFurnaceBlock {
    public IronAutoFurnaceBlock(Settings settings) {
        super(settings);
    }

    @Nullable
    @Override
    public BlockEntity createBlockEntity(BlockPos pos, BlockState state) {
        return new IronAutoFurnaceBlockEntity(pos, state);
    }

    @Nullable
    @Override
    public <T extends BlockEntity> BlockEntityTicker<T> getTicker(World world, BlockState state, BlockEntityType<T> type) {
        return world.isClient ? checkType(type, AutoWorkstations.IRON_AUTO_FURNACE_BLOCK_ENTITY, IronAutoFurnaceBlockEntity::clientTick) : checkType(type, AutoWorkstations.IRON_AUTO_FURNACE_BLOCK_ENTITY, IronAutoFurnaceBlockEntity::serverTick);
    }

    @Override
    public void appendTooltip(ItemStack itemStack, BlockView world, List<Text> tooltip, TooltipContext tooltipContext) {
        tooltip.add( translatableText("block.autoworkstations.iron_auto_furnace.tooltip").formatted(Formatting.ITALIC, Formatting.DARK_PURPLE) );
        tooltip.add( translatableText("block.autoworkstations.hopper_compatible.tooltip").formatted(Formatting.ITALIC, Formatting.DARK_PURPLE) );
    }
}
