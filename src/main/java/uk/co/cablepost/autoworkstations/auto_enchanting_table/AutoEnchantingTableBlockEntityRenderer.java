package uk.co.cablepost.autoworkstations.auto_enchanting_table;

import net.minecraft.block.BlockState;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.client.render.VertexConsumer;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.block.entity.BlockEntityRenderer;
import net.minecraft.client.render.block.entity.BlockEntityRendererFactory;
import net.minecraft.client.render.entity.model.BookModel;
import net.minecraft.client.render.entity.model.EntityModelLayers;
import net.minecraft.client.render.model.json.ModelTransformation;
import net.minecraft.client.texture.SpriteAtlasTexture;
import net.minecraft.client.util.SpriteIdentifier;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.RotationAxis;
import org.joml.Vector3f;
import uk.co.cablepost.autoworkstations.AutoWorkstations;

import java.util.Objects;

public class AutoEnchantingTableBlockEntityRenderer<T extends AutoEnchantingTableBlockEntity> implements BlockEntityRenderer<T> {

    public static final SpriteIdentifier BOOK_TEXTURE = new SpriteIdentifier(SpriteAtlasTexture.BLOCK_ATLAS_TEXTURE, new Identifier("entity/enchanting_table_book"));
    private final BookModel book;

    public AutoEnchantingTableBlockEntityRenderer(BlockEntityRendererFactory.Context context) {
        super();
        book = new BookModel(context.getLayerModelPart(EntityModelLayers.BOOK));
    }

    @Override
    public void render(AutoEnchantingTableBlockEntity blockEntity, float tickDelta, MatrixStack matrices, VertexConsumerProvider vertexConsumers, int light, int overlay) {
        matrices.push();


        // --- book start ---

        matrices.translate(0.5, 0.79, 0.5);
        this.book.setPageAngles(1f, 1f, 1f, 1f);
        VertexConsumer vertexConsumer = BOOK_TEXTURE.getVertexConsumer(vertexConsumers, RenderLayer::getEntitySolid);
        matrices.multiply(RotationAxis.POSITIVE_Z.rotationDegrees(84.0f));
        this.book.renderBook(matrices, vertexConsumer, 255, overlay, 1.0f, 1.0f, 1.0f, 1.0f);
        matrices.multiply(RotationAxis.POSITIVE_Z.rotationDegrees(-84.0f));
        matrices.translate(-0.5, -0.79, -0.5);

        // ---  book end  ---

        if(blockEntity.expLevel > 0) {

            // --- xp inside start ---
            ItemStack xpInside = new ItemStack(AutoWorkstations.AUTO_ENCHANTING_TABLE_XP_INSIDE_BLOCK);
            xpInside.addEnchantment(Enchantment.byRawId(0), 1);

            float xpInsideUpAmt = 0.55f + (0.45f * Math.min(blockEntity.expLevel / 30f, 1f));//0.55 - 1.0


            matrices.translate(0.5, 0.5f, 0.5f);
            matrices.scale(0.9f, 0.9f * xpInsideUpAmt, 0.9f);
            MinecraftClient.getInstance().getItemRenderer().renderItem(xpInside, ModelTransformation.Mode.NONE, light, overlay, matrices, vertexConsumers, 0);
            matrices.scale(1f / 0.9f, 1f / (0.9f * xpInsideUpAmt), 1f / 0.9f);
            matrices.translate(-0.5f, -0.5f, -0.5f);
            // ---  xp inside end  ---

            // --- green floor inside start ---
            ItemStack greenFloor = new ItemStack(Items.LIME_CONCRETE);

            matrices.translate(0.5, 0.67f, 0.5f);
            matrices.scale(0.97f, 0.1f, 0.97f);
            MinecraftClient.getInstance().getItemRenderer().renderItem(greenFloor, ModelTransformation.Mode.NONE, 100, overlay, matrices, vertexConsumers, 0);
            matrices.scale(1f / 0.97f, 10f, 1f / 0.97f);
            matrices.translate(-0.5f, -0.67f, -0.5f);
            // ---  green floor inside end  ---

        }

        matrices.pop();
    }
}
